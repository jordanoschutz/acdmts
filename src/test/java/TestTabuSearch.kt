import heuristic.Solution
import heuristic.TabuSearch
import utils.InputParser
import kotlin.test.assertFalse
import kotlin.test.assertTrue

/**
 * Entry point dos testes unitários da Busca Tabu.
 */
fun main() {
    testStartSolution()
    testNeighborhood()
}

/**
 * Testa se a solução inicial gera tempos de início que respeitam a restrição
 * de distância mínima do ACDM.
 */
fun testStartSolution() {
    val rawSolution = Solution(getInstanceTest())
    assertFalse(rawSolution.isValid())

    val solution = TabuSearch.generateStartSolution(getInstanceTest())
    assertTrue(solution.isValid())

    println("Makespan Sol. 1 = ${solution.objectiveValue()}")
}

/**
 * Testa a geração de vizinhança de uma solução.
 */
fun testNeighborhood() {
    val startSolution = TabuSearch.generateStartSolution(getInstanceTest())
    assertTrue(startSolution.isValid())

    val neighbors = TabuSearch.neighborhoodOf(startSolution, 3, 3)
    assertTrue(neighbors.all { it.solution.isValid() })

    println("Inicial - makespan = ${startSolution.objectiveValue()}")
    neighbors.forEachIndexed { index, neighbor ->
        println("Vizinho $index - makespan = ${neighbor.solution.objectiveValue()}")
    }
}

private fun getInstanceTest() = InputParser.parseInstance("trsp_50_1")

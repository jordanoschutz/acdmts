package entities

/**
 * Representa uma tarefa.
 */
data class Task(
    /**
     * Identificador da tarefa.
     */
    val id: Int,
    /**
     * Duração da tarefa em unidade de tempo abstrata.
     */
    val duration: Int,
    /**
     * Tempo de início da tarefa.
     */
    var startTime: Int = 0
) {
    /**
     * Duração total de uma tarefa.
     */
    fun makespan() = startTime + duration

    /**
     * Representação String de uma tarefa: mostra todas as propriedades mais o tempo de término em pares chave-valor
     * separados por ";".
     */
    override fun toString() = "Tarefa=$id;Duração=$duration;Tempo de início=$startTime;Término=${makespan()}"
}

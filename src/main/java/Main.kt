@file:Suppress("unused")

import heuristic.TabuSearch
import org.json.JSONObject
import utils.InputParser
import java.io.File

/**
 * Entry point da aplicação de meta-heurística Tabu para o Agendamento com distâncias mínimas.
 */
fun main() {
    runVaryingParams()
}

fun runVaryingParams() {
    val nameOfInstance = "trsp_50_1"
    val instance = InputParser.parseInstance(nameOfInstance)
    var testsCount = 1
    val maxTests = 4
    val timeoutMillis = 300000L /* 5 minutos */
    val outJson = JSONObject()

    while (testsCount <= maxTests) {
        val permutationsCount = testsCount
        val neighborhoodSize = testsCount + 1
        val tabuListSize = testsCount * 10

        println("Teste com perm = $permutationsCount, viz = $neighborhoodSize, tabu = $tabuListSize")

        val searchResult = TabuSearch.search(
            instance,
            tabuListSize,
            neighborhoodSize,
            permutationsCount,
            timeoutMillis,
            true
        )

        outJson.append(
            nameOfInstance,
            JSONObject()
                .put("perm", permutationsCount)
                .put("neighborhood", neighborhoodSize)
                .put("tabuListSize", tabuListSize)
                .put("iterations", searchResult.iterations)
                .put("bestSolution", searchResult.bestSolution.objectiveValue())
                .put("executionTimeMinutes", searchResult.executionTimeMillis.toDouble() / 1000.0 / 60.0)
                .put("tabuListSize", searchResult.sizeofTabuList)
                .put("neighborhoodSize", searchResult.sizeofNeighborhood)
                .put("permutations", searchResult.permutationsForNeighborhood)
                .put("startSolution", searchResult.startSolution.objectiveValue())
                .put(
                    "startSchedule",
                    searchResult.startSolution.tasks.joinToString { "${it.id};${it.startTime};${it.duration}" }
                )
                .put(
                    "optimalSchedule",
                    searchResult.bestSolution.tasks.joinToString { "${it.id};${it.startTime};${it.duration}" }
                )
        )

        ++testsCount
    }

    val f = File("${nameOfInstance}_${System.currentTimeMillis()}.json")
    f.createNewFile()
    f.writeText(outJson.toString(2))
}

/**
 * Executa todos os testes fornecidos para o desenvolvimento da heurística.
 */
fun runAll() {
    val timeoutMillis = 600000L /* 10 minutos */
    val outJson = JSONObject()
    val expectedJson = InputParser.getExpectedJson()

    InputParser.fetchAllInstances()
        .forEach { instanceName ->
            val tasks = InputParser.parseInstance(instanceName)
            val (permutationsCount, neighborhoodSize, tabuListSize) = when (tasks.size) {
                50 -> Triple(1, 3, 20) /* 2 movimentos tabu por iteração */
                100 -> Triple(2, 3, 40) /* 4 movimentos tabu por iteração */
                else -> Triple(3, 3, 400) /* 6 movimentos tabu por iteração */
            }

            println("***** Instância: $instanceName *****")

            val searchResult = TabuSearch.search(
                tasks,
                tabuListSize,
                neighborhoodSize,
                permutationsCount,
                timeoutMillis,
                true
            )

            outJson.append(
                instanceName,
                JSONObject()
                    .put("iterations", searchResult.iterations)
                    .put("bestSolution", searchResult.bestSolution.objectiveValue())
                    .put("executionTimeMillis", searchResult.executionTimeMillis)
                    .put("tabuListSize", searchResult.sizeofTabuList)
                    .put("neighborhoodSize", searchResult.sizeofNeighborhood)
                    .put("permutations", searchResult.permutationsForNeighborhood)
                    .put("bestKnownValue", expectedJson.getJSONObject("instances").getLong(instanceName))
            )

            println("*************************")
        }

    val f = File("out_result_${System.currentTimeMillis()}.json")
    f.createNewFile()
    f.writeText(outJson.toString(2))
}

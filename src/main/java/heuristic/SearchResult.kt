package heuristic

/**
 * Representa o resultado da busca tabu.
 */
data class SearchResult (
    /**
     * Número de iterações.
     */
    val iterations: Long,
    /**
     * Melhor solução encontrada.
     */
    val bestSolution: Solution,
    /**
     * Tempo estimado de execução.
     */
    val executionTimeMillis: Long,
    /**
     * Cardinalidade da lista tabu.
     */
    val sizeofTabuList: Int,
    /**
     * Tamanho da vizinhança.
     */
    val sizeofNeighborhood: Int,
    /**
     * Número de permutações na geração da vizinhança.
     */
    val permutationsForNeighborhood: Int,
    /**
     * Solução inicial
     */
    val startSolution: Solution
)

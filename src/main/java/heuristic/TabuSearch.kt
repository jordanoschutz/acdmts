package heuristic

import entities.Task
import kotlin.math.min
import kotlin.random.Random
import kotlin.random.nextInt

/**
 * Representa uma heurística de Busca Tabu.
 */
object TabuSearch {
    /**
     * Executa a busca tabu.
     */
    fun search(
        tasks: Array<Task>,
        sizeofTabuList: Int,
        sizeofNeighborhood: Int,
        permutationsForNeighborhood: Int,
        timeoutMillis: Long,
        printDebug: Boolean
    ): SearchResult {
        val actualStartTime = System.currentTimeMillis()

        var currentSolution = generateStartSolution(tasks)
        var bestSolution = currentSolution
        var iterations = 0L
        val tabuList = createTabuList(sizeofTabuList)
        val startTime = System.currentTimeMillis()

        while (System.currentTimeMillis() - startTime <= timeoutMillis) {
            ++iterations

            val fittestNeighbor =
                neighborhoodOf(currentSolution, sizeofNeighborhood, permutationsForNeighborhood)
                    .filter { neighbor -> tabuList.intersect(neighbor.movedTasksId).isEmpty() }
                    .minBy { it.solution.objectiveValue() }
                    ?: continue

            updateTabuList(tabuList, fittestNeighbor.movedTasksId)

            currentSolution = fittestNeighbor.solution

            when {
                currentSolution < bestSolution -> {
                    if (printDebug) {
                        println("-------------------------")
                        println("Encontrada melhor solução")
                        println("De:   ${bestSolution.objectiveValue()}")
                        println("Para: ${currentSolution.objectiveValue()}")
                        println("-------------------------")
                    }

                    bestSolution = currentSolution
                }
            }
        }

        return SearchResult(
            iterations,
            bestSolution,
            System.currentTimeMillis() - actualStartTime,
            sizeofTabuList,
            sizeofNeighborhood,
            permutationsForNeighborhood,
            generateStartSolution(tasks)
        )
    }

    /**
     * Gera uma solução inicial para a Busca Tabu. O critério de geração é feito em dois passos:
     * 1) As tarefas são ordenadas de forma ascendente pela duração.
     * 2) Com a primeira tarefa no tempo 0, as tarefas são iteradas 2 a 2 e o tempo de início é definido como
     * o tempo de início da primeira tarefa somado ao mínimo obtido pela duração dessas duas tarefas.
     */
    fun generateStartSolution(tasks: Array<Task>): Solution {
        // Adiciona as tarefas ordenadas por duração
        val solution = Solution(tasks.sortedBy { it.duration }.toTypedArray())

        ensureMinimumDistance(solution)

        return solution
    }

    /**
     * Determina soluções válidas vizinhas a solution.
     */
    fun neighborhoodOf(solution: Solution, count: Int, permutations: Int): List<Neighbor> {
        val sourceList = solution.tasks.toList()
        val random = Random(System.currentTimeMillis())
        val neighborhood = mutableListOf<Neighbor>()
        var candidate: Neighbor

        while (neighborhood.size < count) {
            // candidate = Solution(sourceList.shuffled(random).toTypedArray())

            candidate = findNeighbor(sourceList, random, permutations)

            ensureMinimumDistance(candidate.solution)

            if (solution == candidate.solution || neighborhood.contains(candidate)) {
                continue
            }

            neighborhood.add(candidate)
        }

        return neighborhood
    }

    /**
     * Para uma dada solução, garante que a distância mínima entre as tarefas será respeitada.
     */
    private fun ensureMinimumDistance(solution: Solution) {
        solution.tasks.forEach { it.startTime = 0 }

        val stop = solution.tasks.size
        var current: Task

        while (!solution.isValid()) {
            for (i in 0 until stop - 1) {
                current = solution.tasks[i]

                for (t in solution.tasks.slice(i + 1 until stop)) {
                    if (!Solution.hasMinDistance(current, t)) {
                        t.startTime = current.startTime + min(current.duration, t.duration)
                    }
                }
            }
        }

        solution.tasks.sortBy { it.startTime }
    }

    /**
     * Gera um vizinho de uma determinada solução, fazendo uma quantidade de permutações entre posição das tarefas de
     * 10% do tamanho de sourceTasks, escolhendo, via random, os índices a serem modificados.
     */
    private fun findNeighbor(sourceTasks: List<Task>, random: Random, permutationCount: Int): Neighbor {
        val neighbor = Neighbor(Solution(sourceTasks.toTypedArray()))
        var swapCount = permutationCount
        var firstIndex: Int
        var secondIndex: Int
        var aux: Task

        while (swapCount-- > 0) {
            firstIndex = random.nextInt(0 until sourceTasks.size)
            secondIndex = random.nextInt(0 until sourceTasks.size)

            aux = neighbor.solution.tasks[firstIndex]
            neighbor.solution.tasks[firstIndex] = neighbor.solution.tasks[secondIndex]
            neighbor.solution.tasks[secondIndex] = aux

            neighbor.movedTasksId.add(neighbor.solution.tasks[firstIndex].id)
            neighbor.movedTasksId.add(neighbor.solution.tasks[secondIndex].id)
        }

        return neighbor
    }

    /**
     * Cria uma nova lista tabu com cardinalidade listSize.
     */
    private fun createTabuList(listSize: Int) = Array((listSize)) { -1 }

    /**
     * Atualiza a lista tabu, de forma circular, com os identificadores das tarefas contidos em withValues.
     */
    private fun updateTabuList(tabuList: Array<Int>, withValues: List<Int>) {
        var nextIndex = tabuList.indexOfFirst { it == -1 }

        if (nextIndex == -1) {
            nextIndex = 0
        }

        withValues.forEach { v ->
            if (nextIndex == tabuList.size) {
                nextIndex = 0
            }

            tabuList[nextIndex++] = v
        }
    }
}

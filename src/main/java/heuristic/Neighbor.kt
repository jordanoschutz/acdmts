package heuristic

/**
 * Representa o vizinho de uma solução.
 */
data class Neighbor(
    /**
     * A solução vizinha.
     */
    val solution: Solution,
    /**
     * O identificador das tarefas que foram modificadas e distinguem o vizinho.
     */
    val movedTasksId: MutableList<Int> = mutableListOf()
)

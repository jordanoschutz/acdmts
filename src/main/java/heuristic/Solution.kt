package heuristic

import entities.Task
import kotlin.math.abs
import kotlin.math.min

/**
 * Representa uma solução da Busca Tabu.
 */
class Solution(val tasks: Array<Task>) : Comparable<Solution> {

    init {
        for (i in 0 until tasks.size) {
            tasks[i] = Task(tasks[i].id, tasks[i].duration, tasks[i].startTime)
        }
    }

    /**
     * Calcula o valor do makespan da solução.
     */
    fun objectiveValue() = taskWithMaxMakespan().makespan()

    /**
     * Determina se a solução é válida. Uma solução é válida se, e somente se:
     * para cada par de tarefas (t1, t2), |t1.startTime - t2.startTime| >= min{t1.duration, t2.duration}.
     */
    fun isValid(): Boolean {
        var current: Task
        val stop = tasks.size

        for (i in 0 until stop - 1) {
            current = tasks[i]

            for (t in tasks.slice(i + 1 until stop)) {
                if (!hasMinDistance(current, t)) {
                    return false
                }
            }
        }

        return true
    }

    /**
     * Determina se uma solução é maior que, menor que ou igual a uma outra solução, de acordo com o valor
     * da função objetivo.
     */
    override fun compareTo(other: Solution) = objectiveValue().compareTo(other.objectiveValue())

    /**
     * Determina se instância é igual a outro objeto. other é igual a instância atual se, e somente se, other é uma
     * instância de Solution não nula e o valor da função objetivo de other é igual ao valor da função objetivo
     * da instância atual.
     */
    override fun equals(other: Any?) = other is Solution && other.objectiveValue() == objectiveValue()

    /**
     * hashCode
     */
    @Suppress("RedundantOverride")
    override fun hashCode() = super.hashCode()

    /**
     * Recupera a tarefa que tem o maior tempo de duração.
     */
    private fun taskWithMaxMakespan() = tasks.maxBy { it.makespan() }!!

    /**
     * Static
     */
    companion object {
        /**
         * Determina se a distância mínima entre as tarefas é respeitada. Isso ocorre se, e somente se,
         * |t1.startTime - t2.startTime| >= min{t1.duration, t2.duration}.
         */
        fun hasMinDistance(t1: Task, t2: Task) = abs(t1.startTime - t2.startTime) >= min(t1.duration, t2.duration)
    }
}

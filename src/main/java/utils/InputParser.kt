package utils

import entities.Task
import org.json.JSONObject
import java.io.File
import java.io.FileNotFoundException

/**
 * Parser para mapeamento de instâncias de entrada.
 */
object InputParser {
    /**
     * Diretório fonte das instâncias.
     */
    private const val INSTANCES_DIR = "src/main/resources/instances"
    /**
     * Extensão dos arquivos da instância.
     */
    private const val INSTANCE_FILE_EXT = "dat"

    /**
     * Dado o nome de uma instância, e.g. "trsp_50_1", cria uma coleção de tarefas a partir
     * da especificação do formato do arquivo de instância.
     */
    fun parseInstance(instanceName: String): Array<Task> {
        val file = File("$INSTANCES_DIR/$instanceName.$INSTANCE_FILE_EXT")

        if (!file.exists()) {
            throw FileNotFoundException("Instância $instanceName não encontrada.")
        }

        val allLines = file.readLines()

        return allLines
            .slice(1 until allLines.size)
            .mapIndexed { index, duration ->
                Task(index, duration.toInt(), 0)
            }
            .toTypedArray()
    }

    /**
     * Recupera todas as instâncias de teste contidas no diretório de resources.
     */
    fun fetchAllInstances(): List<String> =
        File(INSTANCES_DIR)
            .listFiles { pathname: File -> pathname.extension == INSTANCE_FILE_EXT }
            .map { f -> f.nameWithoutExtension }

    /**
     * Recupera o JSON com os resultados esperados por arquivo.
     */
    fun getExpectedJson() = JSONObject(File("$INSTANCES_DIR/expected.json").readText())
}
